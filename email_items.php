<?php
$host = "ri-db.cdvxjcthpejg.eu-central-1.rds.amazonaws.com";
$dbname = "ri_db";
$user = "ri";
$pass = "12345678";
$arr = array();

$email = '';
if(isset($_GET['email']))
{
 $email = $_GET['email'];
}
else{
  die();
}
$del_id = 0;
if(isset($_GET['del']) && isset($_GET['id'])){
  try {
    $del_id = $_GET['id'];
    $DBH = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
    $STH = $DBH->prepare("DELETE FROM urls WHERE id = ?");
    $STH->execute(array($del_id));
  } catch (PDOException $e) {
    echo $e->getMessage();
    die();
  }
  
}

if(isset($_POST['url'])){
  try {
    $url = $_POST['url'];
    $DBH = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
    $STH = $DBH->prepare("SELECT id FROM emails WHERE emails.email = ?");
    $STH->execute(array($email));
    $res = $STH->fetchAll();
    $id = $res[0]["id"];

    $STH = $DBH->prepare("INSERT INTO urls (email_id, url) VALUES(?, ?)");
    $STH->execute(array($id, $url));

  } catch (PDOException $e) {
    echo $e->getMessage();
    die();
  }
}


try {  
  # MySQL через PDO_MYSQL  
  $DBH = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);  

  $STH = $DBH->prepare("SELECT
   urls.url, urls.id FROM urls LEFT JOIN emails
   ON urls.email_id = emails.id
   WHERE emails.email = ?");

  $STH->execute(array($email));
  $arr = $STH->fetchAll();
}  
catch(PDOException $e) {  
  echo $e->getMessage();  
}
?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8" />
	<title>Заголовок</title>
	<meta name="description" content="" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="shortcut icon" href="favicon.png" />
	<link rel="stylesheet" href="css/bootstrap.min.css" />
	<link rel="stylesheet" href="css/main.css" />

	<script src="libs/jquery/jquery-1.11.1.min.js"></script>
	<script src="js/common.js"></script>
  <script src="js/bootstrap.min.js"></script>

</head>
<body>
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="list-group">
          <a href="#" class="list-group-item active"><?php echo $email ?></a>
          <?php foreach ($arr as $key => $value):?> 
            <div class="list-group-item">
              <div class="email-url">
                <a href=" <?php echo $value["url"]?>"> <?php echo $value["url"]?></a>


              </div>
              <a href="?email=<?php echo $email;?>&id=<?php echo $value["id"]?>&del=1" class=" email-button btn btn-danger">DEL</a>
            </div>
          <?php endforeach?>

        </div>
        <form class="form-inline" method="POST">
          <div class="form-group col-md-9 email-input">
            <input type="text" name="url" required class="form-control" id="exampleInputAmount" placeholder="URL">
          </div>
          <div class="col-md-3 button-input">
            <button type="submit" class="btn btn-success">Add Url</button>
          </div>
        </form>
      </div>


    </div>
  </div>          
</body>
</html>