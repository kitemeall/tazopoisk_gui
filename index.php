<?php

$host = "ri-db.cdvxjcthpejg.eu-central-1.rds.amazonaws.com";
$dbname = "ri_db";
$user = "ri";
$pass = "12345678";

if(isset($_POST['email'])){
  try {
    $email = $_POST['email'];
    $DBH = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
    $STH = $DBH->prepare("INSERT INTO emails (email) VALUE (?);");
    $STH->execute(array($email));

  } catch (PDOException $e) {
    echo $e->getMessage();
    die();
  }
}

$arr = array();
try {  
  # MySQL через PDO_MYSQL  
  $DBH = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);  

  $STH = $DBH->query("SELECT email FROM emails"); 
  $STH->setFetchMode(PDO::FETCH_ASSOC);
  $arr = $STH->fetchAll();
  
}  
catch(PDOException $e) {  
  echo $e->getMessage();  
}
?>

<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="utf-8" />
  <title>Заголовок</title>
  <meta name="description" content="" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="shortcut icon" href="favicon.png" />
  <link rel="stylesheet" href="css/bootstrap.min.css" />
  <link rel="stylesheet" href="css/main.css" />

  <script src="libs/jquery/jquery-1.11.1.min.js"></script>
  <script src="js/common.js"></script>
  <script src="js/bootstrap.min.js"></script>

</head>
<body>
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="list-group">
          <a href="#" class="list-group-item active">Emails:</a>
          <?php foreach ($arr as $key => $value): ?>
          <a href="email_items.php?email=<?php echo $value['email']; ?>" class="list-group-item"><?php echo $value['email']; ?></a>
        <?php endforeach; ?>
      </div>
      <form class="form-inline" method="POST">
        <div class="form-group col-md-9 email-input">
          <input type="email" name="email" required class="form-control" id="exampleInputAmount" placeholder="Email">
        </div>
        <div class="col-md-3 button-input">
          <button type="submit" class="btn btn-success">Add Email</button>
        </div>
      </form>
    </div>


  </div>
</div>          
</body>
</html>

